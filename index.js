function iniciar() {
    var elementos = $(".danza");
    for (var i = 0; i < elementos.length; i++) {
        $($(".danza")[i]).addClass("hidden");
    }
    setDanza(1);
}

function setDanza(numeroDanza) {
    var elementos = $(".danza");
    for (var i = 0; i < elementos.length; i++) {
        $($(".danza")[i]).addClass("hidden");
    }
    $("#danza" + numeroDanza).removeClass("hidden");
}

$('.carousel').carousel({
    interval: false
});

$('#carouselExampleIndicators').on('slide.bs.carousel', function (ev) {
    var id = ev.relatedTarget.id;
    switch (id) {
        case "1":
            setDanza(1);
            break;
        case "2":
            setDanza(2);
            break;
        case "3":
            setDanza(3);
            break;
        case "4":
            setDanza(4);
            break;
        case "5":
            setDanza(5);
            break;
        case "6":
            setDanza(6);
            break;
        default:
            console.log("Error");
    }
});

iniciar();