﻿# proyecto-danzas

DESARROLLO COLABORATIVO

Proyecto que muestra las mejores danzas de Bolivia


Ejecutar el archivo index.html

## Integrantes

* **Faviana Pimentel** - *Product Owner*

* **Karen Salazar** - *Scrum Master*

* **Pedro Mercado** - *Teach Lead*

* **Mihael Cruz** - *Developer*

* **Victor Perez** - *Developer*

* **Lenin Aparicio** - *QA Lead*

* **Denis Milan** - *Tester*